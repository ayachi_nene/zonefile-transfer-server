FROM python:3

WORKDIR /home
COPY . .
RUN tar -xzf ./sshpass-1.06.tar.gz && \ 
    cd sshpass-1.06 && \
    ./configure && \
    make && \
    make install && \
    cd .. && \
    rm -rf sshpass-1.06 && \
    rm sshpass-1.06.tar.gz
CMD [ "python3", "./main.py" ]
