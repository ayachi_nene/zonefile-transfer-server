from http.server import HTTPServer, SimpleHTTPRequestHandler
from urllib.parse import urlparse
import ssl
import sys
import os
import time
import subprocess

passwd = "1q2w3e4r"
remote_file_name = "copy-file@root-zone-management:/home/copy-file/root.zone"
target_file_name = './copy-file/root.zone'

def get_root_zone_file(file_name):
    
    cmd = '/usr/local/bin/sshpass -p %s scp -o StrictHostKeyChecking=no %s %s' % (passwd, file_name, target_file_name)
    print('Executing command: %s' % cmd)
    sub = subprocess.Popen(cmd, stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True,close_fds=True, preexec_fn = os.setsid)
    for i in range(10):
        time.sleep(0.5)
        if os.path.exists(target_file_name):
            break
    try:
        os.killpg(sub.pid, subprocess.signal.SIGTERM)
    except:
        pass
    if not os.path.exists(target_file_name):
        exit(1)

class rootzoneFileTransferHandler(SimpleHTTPRequestHandler):
    
    def do_GET(self):
        if self.path == '/api/zonefile/root':
            get_root_zone_file(remote_file_name)
            self.path = '/copy-file/root.zone'
        super().do_GET()


httpd = HTTPServer(('0.0.0.0', 443), rootzoneFileTransferHandler)

httpd.socket = ssl.wrap_socket (httpd.socket, 
        keyfile="./server-cert/server.key", 
        certfile='./server-cert/server.crt', server_side=True)

httpd.serve_forever()

